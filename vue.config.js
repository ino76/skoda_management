// Inside vue.config.js
module.exports = {
    // ...other vue-cli plugin options...
    pwa: {
        name: "Škoda",
        themeColor: '#1a1a1a',
        msTileColor: '#1a1a1a',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: '#1a1a1a',
        workboxPluginMode: 'InjectManifest',
        // configure the workbox plugin
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            // swSrc is required in InjectManifest mode.
            swSrc: './src/registerServiceWorker.js',
            // ...other Workbox options...
        }
    }
}