export default class Place {
    constructor(x, y, width, height, c) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = "black"
        this.borderColor = "#bdb12b";
        this.backgroundColor = "#f5e64259";
        this.colorH = "red"
        this.borderColorH = "red";
        this.backgroundColorH = "#f542511c";

        this._id = Place.ID++;
        this.c = c

        this.name = `place ${this._id}`
    }

    static resetID() {
        Place.ID = 1
    }

    draw() {
        this.c.beginPath();
        this.c.rect(this.x, this.y, this.width, this.height);
        this.c.fillStyle = this.backgroundColor;
        this.c.strokeStyle = this.borderColor;
        this.c.fill();
        this.c.stroke();

        this.c.fillStyle = this.color
        // this.c.strokeStyle = "white";
        this.c.font = "2rem Arial"
        this.c.fillText(this._id, this._id + 1 <= 10 ? this.x + this.width / 2 - 10 : this.x + this.width / 2 - 18, this.y + this.height / 2 + 10);
        // this.c.strokeText(this._id, this._id + 1 < 10 ? this.x + this.width / 2 - 10 : this.x + this.width / 2 - 18, this.y + this.height / 2 + 10);

    }

    highlight() {
        this.c.beginPath();
        this.c.rect(this.x, this.y, this.width, this.height);
        this.c.fillStyle = this.backgroundColorH
        this.c.strokeStyle = this.borderColorH
        this.c.fill();
        this.c.stroke();


        this.c.fillStyle = "red"
        this.c.font = "2rem Arial"
        this.c.fillText(this._id, this._id + 1 <= 10 ? this.x + this.width / 2 - 10 : this.x + this.width / 2 - 18, this.y + this.height / 2 + 10);
    }

    toString() {
        console.log(`log:
        x: ${this.x}
        y: ${this.y}
        width: ${this.width}
        height: ${this.height}`);
    }
}

Place.ID = 1;