import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import NProgress from "nprogress";
import store from "./store/store.js";
import swal from "sweetalert";
import Buefy from "buefy";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: "/",
            name: "home",
            redirect: '/login',
            component: Home,
            meta: { requiresLogin: false }
        },
        {
            path: "/login",
            name: "login",
            component: () => import("./views/Login.vue"),
            meta: { requiresLogin: false }
        },
        {
            path: "/dashboard",
            name: "dashboard",
            component: () => import("./views/Dashboard.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/users",
            name: "users",
            component: () => import("./views/Users.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/knowledge",
            name: "knowledge",
            component: () => import("./views/Knowledge.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/spare-parts",
            name: "spare-parts",
            component: () => import("./views/SpareParts.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/work_notifications",
            name: "work_notifications",
            component: () => import("./views/WorkNotifications.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/work_orders",
            name: "work_orders",
            component: () => import("./views/WorkOrders.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/operations",
            name: "operations",
            component: () => import("./views/Operations.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/completion_confirmations",
            name: "completion_confirmations",
            component: () => import("./views/CompletionConfirmations.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/new_notification",
            name: "new_notification",
            component: () => import("./views/NewNotification.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/malfunctions",
            name: "malfunctions",
            component: () => import("./views/Malfunctions.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/maintenance",
            name: "maintenance",
            component: () => import("./views/Maintenance.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/orders",
            name: "orders",
            component: () => import("./views/Orders.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/structure",
            name: "structure",
            component: () => import("./views/Structure.vue"),
            meta: { requiresLogin: true }
        },
        {
            path: "/documentation",
            name: "documentation",
            component: () => import("./views/Documentation.vue"),
            meta: { requiresLogin: true }
        }
    ]
});

router.beforeEach((routeTo, routeFrom, next) => {
    NProgress.start();

    if (store.state.user.isLogged && routeTo.meta.requiresLogin) {
        next();
    } else if (!routeTo.meta.requiresLogin) {
        next();
    } else {

        // alert("Nejste přihlášen/a")
        swal({
            title: "Error",
            text: "Access denied. Please login before continue to site.",
            icon: "warning",
            button: "Understand"
        });
        if (routeTo.name !== 'login') {
            router.push("/login");
        }
    }
});

router.afterEach(() => {
    NProgress.done();
});

export default router;
