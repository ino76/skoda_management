export default {
    setSearch({ commit }, value) {
        commit("setSearch", value)
    }
};
