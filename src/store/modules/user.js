console.log(process.env.NODE_ENV)
let logged = false
let shotTS = false
if (process.env.NODE_ENV == "development") {
    logged = true
    shotTS = true
}
import { stat } from "fs";

export default {
    namespaced: true,
    state: {
        isLogged: logged,
        showTableSettings: shotTS,
        user: {
            firstName: "Jakub",
            lastName: "Vonasek",
            email: "raziel@dark.net"
        }
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
        },
        setLogin(state, value) {
            state.isLogged = value;
        },
        setShowTableSettings(state, bool) {
            state.showTableSettings = bool
        }
    },
    actions: {
        setShowTableSettings({ commit }, bool) {
            commit("setShowTableSettings", bool)
        },
        loginUser({ commit }, bool) {
            return new Promise((resolve, reject) => {
                try {
                    commit("setLogin", bool);
                    resolve("logged in successfully")
                } catch (e) {
                    reject("Error while login: " + e.message)
                }
            })
        },
        setUser({ commit }, user) {
            commit("SET_USER", user);
        }
    },
    getters: {
        isLogged(state) {
            return state.isLogged;
        }
    }
};
